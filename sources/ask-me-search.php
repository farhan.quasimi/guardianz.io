<?php
if ($wo['loggedin'] == false) {
  header("Location: " . Wo_SeoLink('index.php?link1=welcome'));
  exit();
}

$wo['questions_data'] = array();
$wo['ask-mode']='all';

$wo['description'] = $wo['config']['siteDesc'];
$wo['keywords']    = $wo['config']['siteKeywords'];
$wo['page']        = 'ask_me_search';
$wo['title']       = 'Ask Me Search | ' . $wo['config']['siteTitle'];
$wo['content']     = Wo_LoadPage('ask-me/ask-me');