<?php
if ($wo['loggedin'] == false) {
  header("Location: " . Wo_SeoLink('index.php?link1=welcome'));
  exit();
}

$wo['questions_data'] = GetQuestions(['page' => 'trending']);
$wo['ask-mode']='all';

$wo['description'] = $wo['config']['siteDesc'];
$wo['keywords']    = $wo['config']['siteKeywords'];
$wo['page']        = 'ask_me_trending';
$wo['title']       = 'Ask Me Trending | ' . $wo['config']['siteTitle'];
$wo['content']     = Wo_LoadPage('ask-me/ask-me');