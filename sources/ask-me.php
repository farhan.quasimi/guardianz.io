<?php
if ($wo['loggedin'] == false) {
  header("Location: " . Wo_SeoLink('index.php?link1=welcome'));
  exit();
}

$wo['questions_data'] = GetQuestions(['page' => 'home']);
$wo['ask-mode']='all';

$wo['description'] = $wo['config']['siteDesc'];
$wo['keywords']    = $wo['config']['siteKeywords'];
$wo['page']        = 'ask_me';
$wo['title']       = 'Ask Me | ' . $wo['config']['siteTitle'];
$wo['content']     = Wo_LoadPage('ask-me/ask-me');