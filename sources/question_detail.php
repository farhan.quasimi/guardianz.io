<?php
$username = null;
if( $_GET['id'] == 'null' && is_numeric($_GET['questionid']) ){
    $question = $db->rawQuery('SELECT `username` FROM `'.T_USERS.'` INNER JOIN `'.T_QUESTIONS.'` ON (`'.T_USERS.'`.user_id = `'.T_QUESTIONS.'`.user_id) WHERE `'.T_QUESTIONS.'`.id = '. Wo_Secure($_GET['questionid']));
    
    if($question[0]){
        $username = $question[0]->username;

    }
}

if( isset($_GET['id']) && $_GET['id'] !== 'null'){
    $username = Wo_Secure($_GET['id']);
}

if( $username == null ){
    header("Location: " . Wo_SeoLink('index.php?link1=welcome'));
    exit();
}

$user_data  = $db->where('username', $username)->getOne(T_USERS);

$userdata   = Wo_UserData($user_data->user_id);

$questions_data = array();

$questions_data = $db->where('id', Wo_Secure($_GET['questionid']))->get(T_QUESTIONS, 1);

$wo['questions_data']=$questions_data;
$wo['ask-mode']='single';

$wo['description'] = $wo['config']['siteDesc'];
$wo['keywords']    = $wo['config']['siteKeywords'];
$wo['page']        = 'ask_me';
$wo['title']       = $userdata['name'].' | '. $wo['config']['siteTitle'];
$wo['content']     = Wo_LoadPage('ask-me/question_detail');