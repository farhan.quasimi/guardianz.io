<?php
if ($wo['loggedin'] == false) {
  header("Location: " . Wo_SeoLink('index.php?link1=welcome'));
  exit();
}

$payment = $wo['payment'] = Wo_GetUserPaymentSetting($wo['user']['user_id']);


$wo['description'] = $wo['config']['siteDesc'];
$wo['keywords']    = $wo['config']['siteKeywords'];
$wo['page']        = 'user_payment_setting';
$wo['title']       = 'Payment System Setting';
$wo['content']     = Wo_LoadPage('payment-setting/user-payment-setting');