<?php 

if($f == 'free_plan'){
    $user   = Wo_UserData($wo['user']['user_id']);
    $update_array = array(
        'is_pro' => 1,
        'pro_time' => time(),
        'pro_' => 1,
        'pro_type' => 1,
        'verified' => 1,
    );

    $mysqli       = Wo_UpdateUserData($wo['user']['user_id'], $update_array);

    if($mysqli){
        header("Location: " . Wo_SeoLink('index.php?link1=upgraded'));
        exit();
    }else{
        header("Location: " . Wo_SeoLink('index.php?link1=oops'));
        exit();
    }
    
}
