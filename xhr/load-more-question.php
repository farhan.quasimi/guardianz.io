<?php

if (empty($_POST['last_id'])) {
    $data = array('status' => 404);
    header("Content-type: application/json");
        echo json_encode($data);
        exit();
}

else {
    
    $type = Wo_Secure($_GET['first']);
    $id = Wo_Secure($_POST['last_id']);
    $final = '';
    $user_id = 0;
    if (!empty($_POST['user_id'])) {
        $user_id = Wo_Secure($_POST['user_id']);
    }
    
    if ($s == 'questions') {

        
        $final = '';
        $sql = '';
        $loadmore_mode = Wo_Secure($_POST['loadmore_mode']);
        $questions_data = array();
        if($loadmore_mode == 'timeline'){
            $ids = $_POST['ids'];
            $query_otin = '';
            if(!empty($ids) && is_array($ids)){
                $query_otin .= "`id` NOT IN (" . implode(',',$ids) .") ";
            }
            $questions_data = $db->where('user_id', $user_id)
                                 ->where('is_anonymously','0')
                                 ->where('replay_question_id','0')
                                 ->where('public','1')
                                 ->where('id', $id, '<')
                                 ->where($query_otin)
                                 ->orderBy('promoted', 'DESC')
                                 ->orderby('id','DESC')->get(T_QUESTIONS, 20);
        }else if($loadmore_mode == 'home'){
            $ids = $_POST['ids'];
            $questions_data = GetQuestions(['page' => 'home', 'after_post_id' => $id, 'ids' => $ids]);
        }else if($loadmore_mode == 'trending'){
            $ids = $_POST['ids'];
            $wo['ask-mode']='all';
            $questions_data = GetQuestions(['page' => 'trending', 'before_post_id' => $id, 'ids' => $ids]);


        }else if($loadmore_mode == 'hashtag'){
            if(isset($_POST['hashtag']) && !empty($_POST['hashtag'])){
                $ids = $_POST['ids'];
                $questions_data = GetHashtagPosts(Wo_Secure($_POST['hashtag']),$id, 5, null,$ids);
            }
        }
        else if($loadmore_mode == 'search'){
            if(isset($_POST['hashtag']) && !empty($_POST['hashtag'])){
                $ids = array();
                $wo['ask-mode']='all';
                $questions_data = GetSearchPosts(Wo_Secure($_POST['hashtag']),$id, 5, null,$ids);
            }
        }
        foreach ($questions_data as $key => $question){
            if($question->user_id == $user->id){
                $question->isowner = true;
            }else{
                $question->isowner = false;
            }
            $wo['question']=$question;
            $final .= Wo_LoadPage('ask-me/question');
        }
        
                $data = array(
                    'status' => 200,
                    'html' => $final,
                );

        header("Content-type: application/json");
        echo json_encode($data);
        exit();
    }
   
}