<?php 

if ($f == 'purchase_product') {
    if ($s == 'get-user-payment-account') {
        $error = "";
        if (!isset($_GET['amount']) || !is_numeric($_GET['amount']) || $_GET['amount'] < 1) {
            $error = $error_icon . $wo['lang']['please_check_details'];
        }
        if (empty($error)) {
            $data = Wo_GetUserPaymentDetails($_GET['productID'],$_GET['productUserID'],$_GET['amount'],'product');
            header("Content-type: application/json");
            echo json_encode($data);
            exit();
        } else {
            header("Content-type: application/json");
            echo json_encode(array(
                'status' => 500,
                'error' => $error
            ));
            exit();
        }
       
    }
    if ($s == 'get-product-paid') {
        
        if (isset($_GET['success']) && $_GET['success'] == 1 && isset($_GET['paymentId']) && isset($_GET['PayerID'])) {
            if (!is_array(Wo_GetProductPaymentDone($_GET['paymentId'], $_GET['PayerID'], $_GET['productUserID']))) {
                if (Wo_UpdateMyProduct($_GET['myproductID'], $_GET['productUserID'],'product')) {
                    $_GET['amount'] = Wo_Secure($_GET['amount']);


                    $user_data = Wo_UserData($_GET['productUserID']);
                    $proInfo=Wo_GetProInfoByID($user_data['pro_type']);

                    if($proInfo['commission']){
                        $commission=$proInfo['commission'];
                        $adminAmount=($_GET['amount']*$commission)/100;
                        $balance=$_GET['amount']-$adminAmount;
                        Wo_UpdateBalance($_GET['productUserID'], $balance, '+');
                    }else{
                        Wo_UpdateBalance($_GET['productUserID'], $_GET['amount'], '+');
                    }

                    $create_payment_log = mysqli_query($sqlConnect, "INSERT INTO " . T_PAYMENT_TRANSACTIONS . " (`seller_id`,`userid`, `kind`, `amount`, `notes`) VALUES ('" . $_GET['productUserID'] . "', '" . $wo['user']['id'] . "', 'Purchase Product', '" . $_GET['amount'] . "', 'PayPal')");
                    //$_SESSION['product_amount'] = $_GET['amount'];

                    $product=Wo_GetProduct($_GET['myproductID']);
                    if($product['group_username']){
                        $groupid=Wo_GroupIdFromGroupname($product['group_username']);
                        Wo_RegsiterGroupAdd($wo['user']['id'], $groupid);
                    }

                $wo['product_name']=$product['name'];
                $wo['product_price']=$product['price'].' '.$wo['currencies'][$product['currency']]['text'];
                $wo['user_name']=$wo['user']['name'];
                $wo['payment_type']='PayPal';
                $wo['notification_msg']='Thank you for buying a new product!';
                $wo['product_url']=$product['url'];

                $email_body       = Wo_LoadPage('emails/payment-notify');
                $send_message_data       = array(
                    'from_email' => $wo['config']['siteEmail'],
                    'from_name' => $wo['config']['siteName'],
                    'to_email' => $wo['user']['email'],
                    'to_name' => $wo['user']['name'],
                    'subject' => 'Thank you for buying a new product',
                    'charSet' => 'utf-8',
                    'message_body' => $email_body,
                    'is_html' => true
                );
                Wo_SendMessage($send_message_data);

                $wo['product_name']=$product['name'];
                $wo['product_price']=$product['price'].' '.$wo['currencies'][$product['currency']]['text'];
                $wo['user_name']=$user_data['first_name'];
                $wo['payment_type']='PayPal';
                $wo['notification_msg']=$wo['user']['name'].' has purchased a new product!';
                $wo['product_url']=$product['url'];

                $email_body_seller       = Wo_LoadPage('emails/payment-notify');
                $send_message_data_seller       = array(
                    'from_email' => $wo['config']['siteEmail'],
                    'from_name' => $wo['config']['siteName'],
                    'to_email' => $user_data['email'],
                    'to_name' => $user_data['first_name'],
                    'subject' => $wo['notification_msg'],
                    'charSet' => 'utf-8',
                    'message_body' => $email_body_seller,
                    'is_html' => true
                );
                Wo_SendMessage($send_message_data_seller);

                    header("Location: " . Wo_SeoLink('index.php?link1=my-purchases'));
                    exit();
                } else {
                    header("Location: " . Wo_SeoLink('index.php?link1=my-purchases'));
                    exit();
                }
            } else {
                header("Location: " . Wo_SeoLink('index.php?link1=my-purchases'));
                exit();
            }
        } else if (isset($_GET['success']) && $_GET['success'] == 0) {
            header("Location: " . Wo_SeoLink('index.php?link1=my-purchases'));
            exit();
        } else {
            header("Location: " . Wo_SeoLink('index.php?link1=my-purchases'));
            exit();
        }
    }
    if ($s == 'remove' && isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0) {
        $data['status'] = 304;
        if (Wo_DeleteUserAd($_GET['id'])) {
            $data['status'] = 200;
        }
        header("Content-type: application/json");
        echo json_encode($data);
        exit();
    }
    if ($s == 'send' && $wo['loggedin'] === true) {
        $data     = array(
            'status' => 400
        );
        $user_id  = (!empty($_POST['user_id']) && is_numeric($_POST['user_id'])) ? $_POST['user_id'] : 0;
        $amount   = (!empty($_POST['amount']) && is_numeric($_POST['amount'])) ? $_POST['amount'] : 0;
        $userdata = $db->where('user_id', $user_id)->where('active', '1')->getOne(T_USERS);
        $wallet   = $wo['user']['wallet'];
        if (empty($user_id) || empty($amount) || empty($userdata) || empty(floatval($wallet)) || $amount < 0) {
            $data['message'] = $wo['lang']['please_check_details'];
        } else if ($wallet < $amount) {
            $data['message'] = $wo['lang']['amount_exceded'];
        } else {
            $amount          = ($amount <= $wallet) ? $amount : $wallet;
            $up_data1        = array(
                'wallet' => sprintf('%.2f', $userdata->wallet + $amount)
            );
            $up_data2        = array(
                'wallet' => sprintf('%.2f', $wallet - $amount)
            );
            $recipient_name  = $userdata->username;
            $currency        = Wo_GetCurrency($wo['config']['ads_currency']);
            $success_msg     = $wo['lang']['money_sent_to'];
            $notif_msg       = $wo['lang']['sent_you'];
            $data['status']  = 200;
            $data['message'] = "$success_msg@ $recipient_name";
            $db->where('user_id', $user_id)->update(T_USERS, $up_data1);
            $db->where('user_id', $wo['user']['id'])->update(T_USERS, $up_data2);
            $notification_data_array = array(
                'recipient_id' => $user_id,
                'type' => 'sent_u_money',
                'user_id' => $wo['user']['id'],
                'text' => "$notif_msg $amount$currency!",
                'url' => 'index.php?link1=wallet'
            );
            Wo_RegisterNotification($notification_data_array);
        }
        header("Content-type: application/json");
        echo json_encode($data);
        exit();
    }
}
