<?php 
if ($f == "questions") {
    if ($s == 'create_normal' && Wo_CheckSession($hash_id) === true) {
            
            if (empty($_POST['question'])) {
                $errors[] = $error_icon . " Please enter your question";
            }

            $user_id = Wo_Secure($_POST['user_id']);

        if (empty($errors)) {
           
                $question_data = RenderQuestion(Wo_Secure($_POST['question']));
                $profile_user_id = Wo_Secure($_POST['profile_user_id']);
                $insert_data         = array(
                    'user_id' => ($_POST['question_type'] == 'me_ask') ? $user_id : $profile_user_id,
                    'shared_user_id' => 0,
                    'shared_question_id' => 0,
                    'ask_user_id' => ($_POST['question_type'] == 'me_ask') ? 0 : $user_id,
                    'ask_question_id' => 0,
                    'replay_user_id' => 0,
                    'replay_question_id' => 0,
                    'is_anonymously' => (isset($_POST['is_anonymously'])) ? 1 : 0,
                    'question' => $question_data['question'],
                    'photo' => null,
                    'type' => 'question',
                    'active' => 1,
                    'public' => (isset($_POST['view_type'])) ? Wo_Secure($_POST['view_type']) : 0,
                    'time' => time(),
                    'lat' => $wo['user']['lat'],
                    'lng' => $wo['user']['lng'],
                    'postLinkTitle'   =>  (!empty($_POST['url_title'])) ? Wo_Secure($_POST['url_title']) : '',
                    'postLink'        =>  (!empty($_POST['url_link'])) ? Wo_Secure($_POST['url_link']) : '',
                    'postLinkImage'   =>  (!empty($_POST['url_image'])) ? Wo_Secure($_POST['url_image']) : '',
                    'postLinkContent' =>  (!empty($_POST['url_content'])) ? Wo_Secure($_POST['url_content']) : '',
                );
            
                $create_question = $db->insert(T_QUESTIONS, $insert_data);
               
        
                if ($create_question) {
                    $insert_data['id'] = $create_question;
                    $insert_data['isowner'] = true;
                   $qd = ToObject($insert_data);
                    // if (isset($question_data['mentions']) && is_array($question_data['mentions']) && !empty($question_data['mentions'])) {
                    //     foreach ($question_data['mentions'] as $mention) {
                    //         $notif_data = array(
                    //             'notifier_id' => $user_id,
                    //             'recipient_id' => $mention,
                    //             'question_id' => $create_question,
                    //             'type' => 'mention_post',
                    //             'url' => ('@' . $wo['user']['username']),
                    //             'time' => time()
                    //         );
                    //         //Notify($notif_data);
                    //     }
                    // }
                    // if(isset($_POST['question_type']) && $_POST['question_type'] == 'user_ask'){
                    //     $notif_data = array(
                    //         'notifier_id' => ($_POST['question_type'] == 'me_ask') ? $profile_user_id : $user_id,
                    //         'recipient_id' => ($_POST['question_type'] == 'me_ask') ? $user_id : $profile_user_id,
                    //         'question_id' => $create_question,
                    //         'type' => 'user_ask',
                    //         'url' => ('@' . $wo['user']['username']),
                    //         'time' => time()
                    //     );
                    //     //Notify($notif_data);
                    // }
                    $wo['question']=QuestionData($qd);
                    $data = array(
                        'status' => 200,
                        'question_html' => Wo_LoadPage('ask-me/question'),
                        'message' => 'your question was successfully posted'
                    );
                }
        
            
        } else {
            $data = array(
                'errors' => $errors
            );
        }

        header("Content-type: application/json");
        echo json_encode($data);
        exit();
    }

    if ($s == 'create_photo' && Wo_CheckSession($hash_id) === true) {
        
       
        $user_id = Wo_Secure($_POST['user_id']);
        $question_data = RenderQuestion(Wo_Secure($_POST['question']));
        $photo_poll = array();
        if (!empty($_FILES['choice1_img']['tmp_name'])) {
            $file_info1 = array(
                'file' => $_FILES['choice1_img']['tmp_name'],
                'size' => $_FILES['choice1_img']['size'],
                'name' => $_FILES['choice1_img']['name'],
                'type' => $_FILES['choice1_img']['type'],
                'crop' => array('width' => 213, 'height' => 280)
            );
            $file_upload1 = Wo_ShareFile($file_info1,0);
            if (!empty($file_upload1['filename'])) {
                $photo_poll['choice1_id'] = substr(sha1(rand(111, 999)), 0, 20);
                $photo_poll['choice1_img'] = $file_upload1['filename'];
                $photo_poll['choice1_url'] = Wo_GetMedia($file_upload1['filename']);
            }
        }
        if (!empty($_FILES['choice2_img']['tmp_name'])) {
            $file_info2 = array(
                'file' => $_FILES['choice2_img']['tmp_name'],
                'size' => $_FILES['choice2_img']['size'],
                'name' => $_FILES['choice2_img']['name'],
                'type' => $_FILES['choice2_img']['type'],
                'crop' => array('width' => 213, 'height' => 280)
            );
            $file_upload2 = Wo_ShareFile($file_info2,0);
            if (!empty($file_upload2['filename'])) {
                $photo_poll['choice2_id'] = substr(sha1(rand(111, 999)), 0, 20);
                $photo_poll['choice2_img'] = $file_upload2['filename'];
                $photo_poll['choice2_url'] = Wo_GetMedia($file_upload2['filename']);
            }
        }

        $insert_data         = array(
            'user_id' => $user_id,
            'shared_user_id' => 0,
            'shared_question_id' => 0,
            'ask_user_id' => 0,
            'ask_question_id' => 0,
            'replay_user_id' => 0,
            'replay_question_id' => 0,
            'is_anonymously' => ($_POST['is_anonymously'] == 'true') ? 1 : 0,
            'question' => $question_data['question'],
            'photo' => json_encode($photo_poll),
            'type' => 'photo_poll',
            'active' => 1,
            'public' => (isset($_POST['view_type'])) ? Wo_Secure($_POST['view_type']) : 0,
            'time' => time(),
            'lat' => $wo['user']['lat'],
            'lng' => $wo['user']['lng'],
        );

        $create_question = $db->insert(T_QUESTIONS, $insert_data);
        if ($create_question) {
            $insert_data['id'] = $create_question;
            $insert_data['isowner'] = true;
            $qd = ToObject($insert_data);
            // if (isset($question_data['mentions']) && is_array($question_data['mentions']) && !empty($question_data['mentions'])) {
            //     foreach ($question_data['mentions'] as $mention) {
            //         $notif_data = array(
            //             'notifier_id' => $user_id,
            //             'recipient_id' => $mention,
            //             'question_id' => $create_question,
            //             'type' => 'mention_post',
            //             'url' => ('@' . $ask->user->username),
            //             'time' => time()
            //         );
            //         Notify($notif_data);
            //     }
            // }
            $wo['question']=QuestionData($qd);
            $data = array(
                'status' => 200,
                'question_html' => Wo_LoadPage('ask-me/question'),
                'message' => 'your question was successfully posted'
            );
            
        }

        header("Content-type: application/json");
        echo json_encode($data);
        exit();

    }

    if ($s == 'edit_normal' && Wo_CheckSession($hash_id) === true) {

        if (empty($_POST['question'])) {
            $errors[] = $error_icon . " Please enter your question";
        }

        $user_id = Wo_Secure($_POST['user_id']);

        if (empty($errors)) {

        $is_owner    = false;
        $question_id = Wo_Secure($_POST['question_id']);
        $question = RenderQuestion(Wo_Secure($_POST['question']),false);
        $question_data = $db->where('id', $question_id)->getOne(T_QUESTIONS);
        if (!empty($question_data)) {
            if( $question_data->user_id == $user_id){
                $is_owner = true;
            }
        }
        if ($is_owner === true || Wo_IsAdmin() == true) {
            $is_anonymously = ($_POST['is_anonymously'] == 1) ? 1 : 0;
            $update_question = $db->where('id', $question_id)->update(T_QUESTIONS, array('question'=>$question['question'],'is_anonymously' => $is_anonymously));
            if ($update_question) {
                $question_data->question = $question['question'];
                $question_data->is_anonymously = $is_anonymously;
                $qd = ToObject($question_data);
                $question_data->isowner = true;
                // if (isset($question['mentions']) && is_array($question['mentions']) && !empty($question['mentions'])) {
                //     $db->where('notifier_id', $user_id)->where('question_id', $question_id)->where('type', 'mention_post')->delete(T_NOTIFICATIONS);
                //     foreach ($question['mentions'] as $mention) {
                //         $notif_data = array(
                //             'notifier_id' => $user_id,
                //             'recipient_id' => $mention,
                //             'question_id' => $question_id,
                //             'type' => 'mention_post',
                //             'url' => ('@' . $ask->user->username),
                //             'time' => time()
                //         );
                //         Notify($notif_data);
                //     }
                // }
                // $ask->mode = 'single';
                $q = QuestionData($qd);
                $msg = '';
                if(in_array('reply',$q->post_type)) {
                    $msg = 'reply updated successfully';
                }else if(in_array('answer',$q->post_type)) {
                    $msg = 'answer updated successfully';
                }else{
                    $msg = 'question updated successfully';
                }
               
                $wo['question']=$q;
                $data = array(
                    'status' => 200,
                    'question_html' => Wo_LoadPage('ask-me/question'),
                    'message' => $msg
                );
            }
        }
        } else {
            $data = array(
                'errors' => $errors
            );
        }

        header("Content-type: application/json");
        echo json_encode($data);
        exit();
    }

    if ($s == 'delete' && Wo_CheckSession($hash_id) === true) {
        
        if (empty($_POST['question_id'])) {
            $errors[] = $error_icon .'error while delete question';

            $data = array(
                'errors' => $errors
            );
        }
    
        else {
            $is_owner    = false;
            $question_id = Wo_Secure($_POST['question_id']);
            $user_id = Wo_Secure($_POST['user_id']);
    
    
            $question_data = $db->where('id', $question_id)->getOne(T_QUESTIONS);
            if (!empty($question_data)) {
                if( $question_data->user_id == $user_id){
                    $is_owner = true;
                }
            }
            if ($is_owner === true || Wo_IsAdmin() == true) {
    
                if (DeleteQuestion($question_id)) {
                    $data = array(
                        'status' => 200,
                        'message' => 'question deleted successfully'
                    );
                }else{
                    $errors[] = $error_icon . 'error while delete question';

                    $data = array(
                        'errors' => $errors
                    );
                }
    
            }else{
                $errors[] = $error_icon . 'error while delete question';

                $data = array(
                    'errors' => $errors
                );
            }
        }

        header("Content-type: application/json");
        echo json_encode($data);
        exit();
    }

    if ($s == 'like' && Wo_CheckSession($hash_id) === true) {

        
        if (empty($_POST['question_id']) OR empty($_POST['like_user_id'])) {
            $errors[] = $error_icon . 'please check details';

            $data = array(
                'errors' => $errors
            );
        }
    
        else {
            
            $user_id = Wo_Secure($_POST['user_id']);
            $question_id = Wo_Secure($_POST['question_id']);
            $u_id = Wo_Secure($_POST['like_user_id']);
            $is_liked = $db->where('question_id',$question_id)->where('user_id',$user_id)->getValue(T_QUESTIONS_LIKES, 'count(*)');
            $like_created = false;
            
                // $notif_data = array(
                //     'notifier_id' => $user_id,
                //     'recipient_id' => $u_id,
                //     'question_id' => $question_id,
                //     'type' => 'like',
                //     'url' => ('@' . $ask->user->username),
                //     'time' => time()
                // );
    
                if($is_liked === 0){
                    
                    $like_created = $db->insert(T_QUESTIONS_LIKES, array('user_id'=>$user_id,'question_id'=>$question_id));
                    
                    // if( (int)$user_id !== (int)$u_id ) {
                    //     Notify($notif_data);
                    // }
                }else{
                    $like_created = $db->where('question_id',$question_id)->where('user_id',$user_id)->delete(T_QUESTIONS_LIKES);
                    //$like_created = $db->where('recipient_id', $u_id)->where('type', 'like')->where('notifier_id', $user_id)->delete(T_NOTIFICATIONS);
                }
    
                if($like_created){
                    $likes_count = $db->where('question_id',$question_id)->getValue(T_QUESTIONS_LIKES, 'count(*)');
                    $data = array(
                        'status' => 200,
                        'likes_count' => $likes_count,
                        'mode' => ( $is_liked === 0 ) ? 'like' : 'dislike'
                    );
                }
    
        }

        header("Content-type: application/json");
        echo json_encode($data);
        exit();
    }

    if ($s == 'vote' && Wo_CheckSession($hash_id) === true) {

        if ( ( empty($_POST['question_id']) || empty($_POST['choice_id']) || empty($_POST['other_choice_id']) || empty($_POST['user_id']) ) ) {
            $errors[] = $error_icon . 'please check details';

            $data = array(
                'errors' => $errors
            );
        }
        else {
    
            $user_id = Wo_Secure($_POST['user_id']);
            $question_id = Wo_Secure($_POST['question_id']);
            $choice_id = Wo_Secure($_POST['choice_id']);
            $other_choice_id = Wo_Secure($_POST['other_choice_id']);
            if (IsQuestionVoted($question_id) === false) {
                $insert_vote = $db->insert(T_QUESTIONS_VOTES, array('question_id' => $question_id,'choice_id' => $choice_id , 'user_id' => $user_id ,'vote_time' => time()));
                if ($insert_vote) {
                    
                    $data = array(
                        'status' => 200,
                        'votes_count' => QuestionVotes($question_id),
                        'new_percentages' => GetVotePercentages($choice_id, $other_choice_id),
                        'message' => 'question voted successfully'
                    );
                }
            }else{
                $errors[] = $error_icon . 'please check details';
                $data = array(
                    'errors' => $errors
                );
            }
    
        }

        header("Content-type: application/json");
        echo json_encode($data);
        exit();
    }

    if ($s == 'answer' && Wo_CheckSession($hash_id) === true) {

        
        if (empty($_POST['question_id']) OR empty($_POST['question_user_id']) OR empty($_POST['answer'])) {            
            if (empty($_POST['answer'])) {
                $errors[] = $error_icon . " Please enter your answer";

                $data = array(
                    'errors' => $errors
                );
            }
        }
        else {
            
            $user_id = Wo_Secure($_POST['user_id']);
    
            if (Wo_Secure($_POST['question_user_id']) == $user_id) {
                $errors[] = $error_icon . 'please check details';

                $data = array(
                    'errors' => $errors
                );
            }
    
            else{
    
                $question_id = Wo_Secure($_POST['question_id']);
                $question = $db->where('id',$question_id)->getOne(T_QUESTIONS,array('question','is_anonymously','type'));
                $question_user_id = Wo_Secure($_POST['question_user_id']);
                $answer_data = RenderQuestion(Wo_Secure($_POST['answer']));
    
                $insert_data         = array(
                    'user_id' => $user_id,
                    'ask_user_id' => $question_user_id,
                    'ask_question_id' => $question_id,
                    'shared_user_id' => 0,
                    'shared_question_id' => 0,
                    'replay_user_id' => 0,
                    'replay_question_id' => 0,
                    'is_anonymously' => $question->is_anonymously,
                    'question' => htmlspecialchars_decode($answer_data['question']),
                    'photo' => null,
                    'type' => $question->type,
                    'active' => 1,
                    'time' => time(),
                    'lat' => $wo['user']['lat'],
                    'lng' => $wo['user']['lng'],
                );
    
                $create_question = $db->insert(T_QUESTIONS, $insert_data);
                if ($create_question) {
                    $insert_data['id'] = $create_question;
                    $insert_data['isowner'] = true;
                    $qd = ToObject($insert_data);
    
                    // if (isset($answer_data['mentions']) && is_array($answer_data['mentions']) && !empty($answer_data['mentions'])) {
                    //     foreach ($answer_data['mentions'] as $mention) {
                    //         $notif_data = array(
                    //             'notifier_id' => $user_id,
                    //             'recipient_id' => $mention,
                    //             'question_id' => $create_question,
                    //             'type' => 'mention_answer',
                    //             'url' => ('@' . $ask->user->username),
                    //             'time' => time()
                    //         );
                    //         Notify($notif_data);
                    //     }
                    // }
    
                    // $notif_data = array(
                    //     'notifier_id' => $user_id,
                    //     'recipient_id' => $question_user_id,
                    //     'question_id' => $create_question,
                    //     'type' => 'answer_question',
                    //     'url' => ('post/' . $create_question),
                    //     'time' => time()
                    // );
                    // Notify($notif_data);
    
                    $wo['question']=QuestionData($qd);
                    $data = array(
                        'status' => 200,
                        'question_html' => Wo_LoadPage('ask-me/answer'),
                        'message' => 'your question was successfully posted'
                    );
                }
    
            }
    
        }

        header("Content-type: application/json");
        echo json_encode($data);
        exit();
    }

    if ($s == 'replay' && Wo_CheckSession($hash_id) === true) {

        if (empty($_POST['question_id']) OR empty($_POST['question_user_id']) OR empty($_POST['answer'])) {            
            if (empty($_POST['answer'])) {
                $errors[] = $error_icon . " Please enter your answer";

                $data = array(
                    'errors' => $errors
                );
            }
        }
       
        else {

            $user_id = Wo_Secure($_POST['user_id']);
    
            $question_id = Wo_Secure($_POST['question_id']);
            $question = $db->where('id',$question_id)->getOne(T_QUESTIONS,array('question','is_anonymously','type'));
            $question_user_id = Wo_Secure($_POST['question_user_id']);
    
            $ask_user_id = Wo_Secure($_POST['ask_user_id']);
            $ask_question_id = Wo_Secure($_POST['ask_question_id']);
    
            $answer_data = RenderQuestion(Wo_Secure($_POST['answer']));
    
            $insert_data         = array(
                'user_id' => $user_id,
                'ask_user_id' => 0,//$ask_user_id,
                'ask_question_id' => 0,//$ask_question_id,
                'replay_user_id' => $user_id,
                'replay_question_id' => $question_id,
                'shared_user_id' => 0,
                'shared_question_id' => 0,
                'is_anonymously' => $question->is_anonymously,
                'question' => htmlspecialchars_decode($answer_data['question']),
                'photo' => null,
                'type' => $question->type,
                'active' => 1,
                'time' => time()
            );
    
            $create_question = $db->insert(T_QUESTIONS, $insert_data);
            if ($create_question) {
                $insert_data['id'] = $create_question;
                $insert_data['isowner'] = true;
                $insert_data['is_replay'] = true;
                $qd = ToObject($insert_data);
    
                // if (isset($answer_data['mentions']) && is_array($answer_data['mentions']) && !empty($answer_data['mentions'])) {
                //     foreach ($answer_data['mentions'] as $mention) {
                //         $notif_data = array(
                //             'notifier_id' => $user_id,
                //             'recipient_id' => $mention,
                //             'question_id' => $question_id,
                //             'replay_id' => $create_question,
                //             'type' => 'mention_replay',
                //             'url' => ('@' . $ask->user->username),
                //             'time' => time()
                //         );
                //         Notify($notif_data);
                //     }
                // }
    
                // $notif_data = array(
                //     'notifier_id' => $user_id,
                //     'recipient_id' => $question_user_id,
                //     'question_id' => $question_id,
                //     'replay_id' => $create_question,
                //     'type' => 'replay_question',
                //     'url' => ('post/' . $create_question),
                //     'time' => time()
                // );
                // Notify($notif_data);
    

                $wo['question']=QuestionData($qd);
                    $data = array(
                        'status' => 200,
                        'question_html' => Wo_LoadPage('ask-me/question'),
                        'message' => 'done'
                    );
            }
    
        }

        header("Content-type: application/json");
        echo json_encode($data);
        exit();
    }
    
    if ($s == 'report') {
        if (!empty($_GET['post_id'])) {
            $post_data = array(
                'question_id' => $_GET['post_id']
            );
            if (Wo_ReportPost($post_data) == 'unreport') {
                $data = array(
                    'status' => 300,
                    'text' => 'Report This'
                );
            } else {
                $data = array(
                    'status' => 200,
                    'text' => 'Unreport This'
                );
            }
        }
        header("Content-type: application/json");
        echo json_encode($data);
        exit();
    }

    if ($s == 'search' && Wo_CheckSession($hash_id) === true) {
        
    if (empty($_POST['keyword_search'])) {
        $errors[] = $error_icon . 'please check details';
        $data = array(
            'errors' => $errors
        );
    }
    else {
        $keyword_search = Wo_Secure($_POST['keyword_search']);
        
        $questions = $db->where('question','%'.$keyword_search.'%', 'like')
                        ->Where('active', '1')
                        ->orderBy('id', 'DESC')
                        ->get(T_QUESTIONS, 5,array('*'));

        $questions_html = '';
        foreach ($questions as $key => $question){
            $wo['ask-mode']='all';
            if($question->user_id == $user->id){
                $question->isowner = true;
            }else{
                $question->isowner = false;
            }
            $wo['question']=QuestionData($question);
            $questions_html .= Wo_LoadPage('ask-me/question');
        }


        $data = array(
            'status' => 200,
            'questions' => $questions_html,
            'message' => 'done',
            'keyword_search' => $keyword_search,
        );
        
    }
    header("Content-type: application/json");
        echo json_encode($data);
        exit();
    }
}
