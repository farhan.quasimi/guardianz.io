<?php 
if ($f == 'stripe_payment_video') {
 
    include_once('assets/includes/stripe_config.php');
    $product=Wo_GetVideoCourse($_POST['productID']);
    $stripeCurrency=$product['currency'];
    
    if (empty($_POST['stripeToken'])) {
        header("Location: " . Wo_SeoLink('index.php?link1=oops'));
        exit();
    }
    $token = $_POST['stripeToken'];
    try {
        $customer = \Stripe\Customer::create(array(
            'source' => $token
        ));
        $_POST['amount'] = Wo_Secure($_POST['amount']);
        $final_amount = $_POST['amount'] * 100;
        $charge   = \Stripe\Charge::create(array(
            'customer' => $customer->id,
            'amount' => $final_amount,
            'currency' => $stripeCurrency
        ));
        if ($charge) {
            
            $user   = Wo_UserData($wo['user']['user_id']);
            //encrease wallet value with posted amount
            if (Wo_UpdateMyProduct($_POST['productID'], $_POST['productUserID'],'video')) {

                $user_data = Wo_UserData($_POST['productUserID']);
                    $proInfo=Wo_GetProInfoByID($user_data['pro_type']);

                    if($proInfo['commission']){
                        $commission=$proInfo['commission'];
                        $adminAmount=($_POST['amount']*$commission)/100;
                        $balance=$_POST['amount']-$adminAmount;
                        Wo_UpdateBalance($_POST['productUserID'], $balance, '+');
                    }else{
                        Wo_UpdateBalance($_POST['productUserID'], $_POST['amount'], '+');
                    }
               
                $create_payment_log = mysqli_query($sqlConnect, "INSERT INTO " . T_PAYMENT_TRANSACTIONS . " (`seller_id`,`userid`, `kind`, `amount`, `notes`) VALUES ('" . $_POST['productUserID'] . "', '" . $wo['user']['id'] . "', 'Purchase Video Course', '" . $_POST['amount'] . "', 'stripe')");


                $product=Wo_GetVideoCourse($_POST['productID']);
                if($product['group_username']){
                    $groupid=Wo_GroupIdFromGroupname($product['group_username']);
                }else{
                    $groupid=0;
                }

                $wo['product_name']=$product['name'];
            $wo['product_price']=$product['price'].' '.$product['currency'];
            $wo['user_name']=$wo['user']['name'];
            $wo['payment_type']='stripe';
            $wo['notification_msg']='Thank you for buying a new product!';
            $wo['product_url']=Wo_SeoLink("index.php?link1=watch-film&film-id=" . $product['id']);

            $email_body       = Wo_LoadPage('emails/payment-notify');
            $send_message_data       = array(
                'from_email' => $wo['config']['siteEmail'],
                'from_name' => $wo['config']['siteName'],
                'to_email' => $wo['user']['email'],
                'to_name' => $wo['user']['name'],
                'subject' => 'Thank you for buying a new product',
                'charSet' => 'utf-8',
                'message_body' => $email_body,
                'is_html' => true
            );
            Wo_SendMessage($send_message_data);

            $wo['product_name']=$product['name'];
            $wo['product_price']=$product['price'].' '.$product['currency'];
            $wo['user_name']=$user_data['first_name'];
            $wo['payment_type']='stripe';
            $wo['notification_msg']=$wo['user']['name'].' has purchased a new product!';
            $wo['product_url']=Wo_SeoLink("index.php?link1=watch-film&film-id=" . $product['id']);

            $email_body_seller       = Wo_LoadPage('emails/payment-notify');
            $send_message_data_seller       = array(
                'from_email' => $wo['config']['siteEmail'],
                'from_name' => $wo['config']['siteName'],
                'to_email' => $user_data['email'],
                'to_name' => $user_data['first_name'],
                'subject' => $wo['notification_msg'],
                'charSet' => 'utf-8',
                'message_body' => $email_body_seller,
                'is_html' => true
            );
            Wo_SendMessage($send_message_data_seller);
            }
           
            $data = array(
                'status' => 200,
                'location' => Wo_SeoLink('index.php?link1=my-purchases'),
                'userid' => $wo['user']['id'],
                'groupid' => $groupid,
            );
            header("Content-type: application/json");
            echo json_encode($data);
            exit();
        }
    }
    catch (Exception $e) {
        $data = array(
            'status' => 400,
            'error' => $e->getMessage()
        );
        header("Content-type: application/json");
        echo json_encode($data);
        exit();
    }
}
