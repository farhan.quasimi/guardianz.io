<?php 
if ($f == "user_payment_setting") {
    if ($s == 'update_user_payment_setting' && Wo_CheckSession($hash_id) === true) {

        if (empty($_POST['api_key'])) {
            $errors[] = $error_icon . 'Please enter api key';
        } else if (empty($_POST['secret_key'])) {
            $errors[] = $error_icon . 'Please enter Secret key';
        }

        $data_array = array(
            'user_id' => $wo['user']['user_id'],
            'type' => Wo_Secure($_POST['type']),
            'payment_mode' => Wo_Secure($_POST['payment_mode']),
            'currency' => Wo_Secure($_POST['currency']),
            'api_key' => Wo_Secure($_POST['api_key']),
            'secret_key' => Wo_Secure($_POST['secret_key']),
        );

            $pid=Wo_Secure($_POST['pid']);

        Wo_RegisterUserPaymentSetting($data_array,$pid);

        $data = array(
            'status' => 200,
        );

        header("Content-type: application/json");
        if (isset($errors)) {
            echo json_encode(array(
                'errors' => $errors
            ));
        } else {
            echo json_encode($data);
        }
        exit();
    }
}
