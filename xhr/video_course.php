<?php 
if ($f == "video_course") {
    if ($s == 'create' && Wo_CheckSession($hash_id) === true) {
            
            if (strlen($_POST['name']) < 3) {
                $errors[] = $error_icon . " Please enter a valid name";
            }else if (empty($_POST['category'])) {
                $errors[] = $error_icon . " Please choose a category";
            }
            else if (strlen($_POST['description']) < 32) {
                $errors[] = $error_icon . $wo['lang']['desc_more_than32'];
            }
            else if (!file_exists($_FILES["cover"]["tmp_name"])) {
                $errors[] = $error_icon . " Select the cover to the video course";
            }
            else if (empty($_POST['price'])) {
                $errors[] = $error_icon . $wo['lang']['please_choose_price'];
            } 
            else if (!is_numeric($_POST['price'])) {
                $errors[] = $error_icon . $wo['lang']['please_choose_c_price'];
            }
            else if (empty($_POST['video_instructions_links'])) {
                $errors[] = $error_icon . 'Plese add instructions links';
            } 
            else if ($_POST['price'] == '0.00') {
                $errors[] = $error_icon . $wo['lang']['please_choose_price'];
            }
            else if (empty($_FILES["cover"]["tmp_name"])) {
                if (!empty($_FILES["cover"]["error"]) || !empty($_FILES["source"]["error"])) {
                    $errors[] = $error_icon . 'The file is too big, please increase your server upload limit in php.ini';
                } else {
                    $errors[] = $error_icon . $wo['lang']['please_check_details'];
                }
            }
        if (!empty($_FILES["cover"]["tmp_name"])) {
            if (file_exists($_FILES["cover"]["tmp_name"])) {
                $cover = getimagesize($_FILES["cover"]["tmp_name"]);
                if ($cover[0] > 400 || $cover[1] > 570) {
                    $errors[] = $error_icon . " Cover size should not be more than 400x570 ";
                }
            }
        }
        $currency = 0;
        if (isset($_POST['currency'])) {
            if (in_array($_POST['currency'], array_keys($wo['currencies']))) {
                $currency = Wo_Secure($_POST['currency']);
            }
        }
        $price              = Wo_Secure($_POST['price']);
        if (empty($errors)) {
            $registration_data = array(
                'user_id' => $wo['user']['user_id'],
                'name' => Wo_Secure($_POST['name']),
                'genre' => Wo_Secure($_POST['category']),
                'description' => Wo_Secure($_POST['description']),
                'price' => $price,
                'instructions_links' => Wo_Secure($_POST['video_instructions_links']),
                'group_username' => Wo_Secure($_POST['group_username']),
                'iframe' => Wo_Secure($_POST['preview_video']),
                'currency' => $currency,
            );
            $film_id           = Wo_InsertFilm($registration_data);
            if ($film_id && is_numeric($film_id)) {
                $update_film = array();
                if (!empty($_FILES["source"]["tmp_name"]) && empty($_POST['youtube']) && empty($_POST['other'])) {
                    $fileInfo              = array(
                        'file' => $_FILES["source"]["tmp_name"],
                        'name' => $_FILES['source']['name'],
                        'size' => $_FILES["source"]["size"],
                        'type' => $_FILES["source"]["type"],
                        'types' => 'mp4,mov,webm,flv'
                    );
                    $media                 = Wo_ShareFile($fileInfo);
                    $update_film['source'] = $media['filename'];
                }
                if (!empty($_FILES["cover"]["tmp_name"])) {
                    $fileInfo             = array(
                        'file' => $_FILES["cover"]["tmp_name"],
                        'name' => $_FILES['cover']['name'],
                        'size' => $_FILES["cover"]["size"],
                        'type' => $_FILES["cover"]["type"],
                        'types' => 'jpeg,jpg,png,bmp,gif',
                        'compress' => false
                    );
                    $media                = Wo_ShareFile($fileInfo);
                    $update_film['cover'] = $media['filename'];
                }
                if (count($update_film) > 0) {
                    Wo_UpdateFilm($film_id, $update_film);
                    $data = array(
                        'status' => 200,
                        'href' => Wo_SeoLink('index.php?link1=watch-film&film-id=' . $film_id)
                    );
                }
            }
        } else {
            $data = array(
                'errors' => $errors
            );
        }

        header("Content-type: application/json");
        echo json_encode($data);
        exit();
    }

    if ($s == 'edit' && Wo_CheckSession($hash_id) === true) {
        if (strlen($_POST['name']) < 3) {
            $errors[] = $error_icon . " Please enter a valid name";
        }else if (empty($_POST['category'])) {
            $errors[] = $error_icon . " Please choose a category";
        }
        else if (strlen($_POST['description']) < 32) {
            $errors[] = $error_icon . $wo['lang']['desc_more_than32'];
        }
        else if (empty($_POST['price'])) {
            $errors[] = $error_icon . $wo['lang']['please_choose_price'];
        } 
        else if (!is_numeric($_POST['price'])) {
            $errors[] = $error_icon . $wo['lang']['please_choose_c_price'];
        }
        else if (empty($_POST['instructions_links'])) {
            $errors[] = $error_icon . 'Plese add instructions links';
        } 
        else if ($_POST['price'] == '0.00') {
            $errors[] = $error_icon . $wo['lang']['please_choose_price'];
        }
        
    if (!empty($_FILES["cover"]["tmp_name"])) {
        if (file_exists($_FILES["cover"]["tmp_name"])) {
            $cover = getimagesize($_FILES["cover"]["tmp_name"]);
            if ($cover[0] > 400 || $cover[1] > 570) {
                $errors[] = $error_icon . " Cover size should not be more than 400x570 ";
            }
        }
    }
    $currency = 0;
    if (isset($_POST['currency'])) {
        if (in_array($_POST['currency'], array_keys($wo['currencies']))) {
            $currency = Wo_Secure($_POST['currency']);
        }
    }
    $price              = Wo_Secure($_POST['price']);
    if (empty($errors)) {
        $registration_data = array(
            'user_id' => $wo['user']['user_id'],
            'name' => Wo_Secure($_POST['name']),
            'genre' => Wo_Secure($_POST['category']),
            'description' => Wo_Secure($_POST['description']),
            'price' => $price,
            'instructions_links' => $_POST['instructions_links'],
            'group_username' => Wo_Secure($_POST['group_username']),
            'iframe' => Wo_Secure($_POST['preview_video']),
            'currency' => $currency,
        );
        $film_id           = Wo_Secure($_POST['product_id']);
        $film              = Wo_UpdateFilm($film_id, $registration_data);
        if ($film) {
            $update_film = array();
            if (!empty($_FILES["cover"]["tmp_name"])) {
                $fileInfo             = array(
                    'file' => $_FILES["cover"]["tmp_name"],
                    'name' => $_FILES['cover']['name'],
                    'size' => $_FILES["cover"]["size"],
                    'type' => $_FILES["cover"]["type"],
                    'types' => 'jpeg,jpg,png,bmp,gif',
                    'compress' => false
                );
                $media                = Wo_ShareFile($fileInfo);
                $update_film['cover'] = $media['filename'];
            }
            if (count($update_film) > 0) {
                Wo_UpdateFilm($film_id, $update_film);
            }
            $data = array(
                'status' => 200,
                'href' => Wo_SeoLink('index.php?link1=watch-film&film-id=' . $film_id)
            );
        }
    } else {
        $data = array(
            'errors' => $errors
        );
    }

    header("Content-type: application/json");
    echo json_encode($data);
    exit();
    }
    
}
